const webpack = require('webpack');
const path = require('path');
const buildPath = path.resolve(__dirname, 'dist/');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');
const TransferWebpackPlugin = require('transfer-webpack-plugin');

const config = {
  entry: [path.join(__dirname, '/src/app/app.js')],
  resolve: {
    extensions: ["", ".js"],
  },
  devtool: 'source-map',
  output: { path: buildPath, filename: 'app.js', },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: false, },
    }),
    new webpack.NoErrorsPlugin(),
    new TransferWebpackPlugin([ {from: 'www'}, ], path.resolve(__dirname,"src")),
  ],
  module: {
    loaders: [
      { test: /\.js$/, loaders: ['babel-loader'], exclude: [nodeModulesPath], },
      { test: /\.css$/, loader: 'style-loader!css-loader' },
      { test: /\.(woff|woff2|eot|ttf|svg)$/, loader: 'url' },
    ],
  },
  eslint: { configFile: '.eslintrc', },
};

module.exports = config;
