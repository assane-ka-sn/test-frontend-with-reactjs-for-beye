const webpack = require('webpack');
const path = require('path');
const buildPath = path.resolve(__dirname, 'dist/');
const nodeModulesPath = path.resolve(__dirname, 'node_modules');
const TransferWebpackPlugin = require('transfer-webpack-plugin');

const config = {
    //Entry points to the project
    entry: [
        'webpack/hot/dev-server',
        'webpack/hot/only-dev-server',
        path.join(__dirname, '/src/app/app.js'),
    ],
    //Config options on how to interpret requires imports
    resolve: {
        extensions: ["", ".js"],
    },
    //Server Configuration options
    devServer:{
        watchOptions: { poll: 1000 },
        contentBase: 'src/www',  //Relative directory for base of server
        devtool: 'eval',
        hot: true,        //Live-reload
        inline: true,
        port: 3000,        //Port Number
        host: 'localhost',  //Change to '0.0.0.0' for external facing server
    },
    devtool: 'eval',
    output: { path: buildPath, filename: 'app.js', },
    plugins: [
        //Enables Hot Modules Replacement
        new webpack.HotModuleReplacementPlugin(),
        //Allows error warnings but does not stop compiling. Will remove when eslint is added
        new webpack.NoErrorsPlugin(),
        //Moves files
        new TransferWebpackPlugin([{from: 'www'}, ], path.resolve(__dirname, "src")),
    ],
    module: {
        loaders: [
            { test: /\.js$/, loaders: ['react-hot', 'babel-loader'], exclude: [nodeModulesPath] },
            { test: /\.css$/, loader: 'style-loader!css-loader' },
            { test: /\.(woff|woff2|eot|ttf|svg)$/, loader: 'url' },
        ],
    },
    //eslint config options. Part of the eslint-loader package
    eslint: {
        configFile: '.eslintrc',
    },
};

module.exports = config;
