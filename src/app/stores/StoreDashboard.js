import AppDispatcher from '../dispatcher/AppDispatcher';
import AppConstants from '../constants/AppConstants';
import { EventEmitter } from 'events';

const CHANGE_EVENT = 'change';

class StoreDashboardClass extends EventEmitter {

    constructor(){
        super();
        this._products = [];
        this._categories = [];
        this._brands = [];
        this._productsConst = [];
        this._productsConstApi = [];
    }

    getStoreCategories() { return this._categories; }

    getStoreBrands() { return this._brands; }

    getStoreProducts() { return this._products; }

    emitChange() { this.emit(CHANGE_EVENT); }

  	addChangeListener(callback) { this.on(CHANGE_EVENT, callback); }

  	removeChangeListener(callback) { this.removeListener(CHANGE_EVENT, callback); }

}

const StoreDashboard = new StoreDashboardClass();

AppDispatcher.register((action) => {

  switch(action.actionType) {

      case AppConstants.LIST_PRODUCTS_FROM_API:
          StoreDashboard._productsConstApi = action.datas
          if(StoreDashboard._productsConstApi.length!=StoreDashboard._productsConst.length){
              StoreDashboard._productsConst = StoreDashboard._productsConstApi;
              StoreDashboard._products = StoreDashboard._productsConst;
              StoreDashboard.emitChange();
          }
          break;

      case AppConstants.LIST_BRANDS_FROM_API:
          StoreDashboard._brands = action.datas
          StoreDashboard.emitChange();
          break;

      case AppConstants.LIST_CATEGORIES_FROM_API:
          StoreDashboard._categories = action.datas
          StoreDashboard.emitChange();
          break;

      case AppConstants.LIST_PRODUCTS:
          StoreDashboard._products = StoreDashboard._productsConst
          StoreDashboard.emitChange();
          break;

      case AppConstants.RECHERCHE_PRODUCT_DASHBOARD:
          StoreDashboard._products = StoreDashboard._productsConst.filter(product =>
              (
                  product.name.toLowerCase().includes(action.filterProducts.toLowerCase()) ||
                  product.description.toLowerCase().includes(action.filterProducts.toLowerCase()) ||
                  product.brand.name.toLowerCase().includes(action.filterProducts.toLowerCase())
              )
          );
          StoreDashboard.emitChange();
          break;

      case AppConstants.DELETE_PRODUCT:
          StoreDashboard._products = StoreDashboard._products.filter(product => product.id!=action.productId);
          StoreDashboard._productsConst = StoreDashboard._productsConst.filter(product => product.id!=action.productId);
          StoreDashboard.emitChange();
          break;

      case AppConstants.ADD_PRODUCT:
          StoreDashboard.emitChange();
          break;

      case AppConstants.EDIT_PRODUCT:
          StoreDashboard._productsConstApi = action.datas
          StoreDashboard._productsConst = StoreDashboard._productsConstApi;
          StoreDashboard._products = StoreDashboard._productsConst;
          StoreDashboard.emitChange();
          break;

      default:
      // no op
  }

});

export default StoreDashboard;
