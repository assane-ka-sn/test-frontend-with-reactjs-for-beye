import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import injectTapEventPlugin from 'react-tap-event-plugin';
import 'bootstrap/dist/css/bootstrap.min.css';

import Home from './components/home/home'
import Dashboard from './components/dashboard/dashboard'

injectTapEventPlugin();


ReactDOM.render((
  <Router history={browserHistory}>
      <Route path="/" component={Home}>
          <IndexRoute component={Dashboard}/>
          <Route path="/dashboard" component={Dashboard}/>
      </Route>
  </Router>
), document.getElementById('app'))
