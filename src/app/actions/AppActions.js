import AppDispatcher from '../dispatcher/AppDispatcher';
import AppConstants from '../constants/AppConstants';
import axios from 'axios';

const link = "http://localhost:8000";
let intervalTime = null;
const AppActions = {

  /***************************************
  ********* Begin products **********
  ***************************************/

  getActionInitDashboard() {
      this.getActionProductsFromApi()
      intervalTime = setInterval(() =>
          this.getActionProductsFromApi()
      , 10000);
  },

    getActionProducts() {
        AppDispatcher.dispatch({
            actionType: AppConstants.LIST_PRODUCTS,
        });
    },

    getActionRechercheProductsDashboard(filterProducts) {
      clearInterval(intervalTime)
      if(filterProducts.length==0){
          this.getActionProducts()
          intervalTime = setInterval(() =>
              this.getActionProductsFromApi()
          , 1000);
      }
      else {
          AppDispatcher.dispatch({
              actionType: AppConstants.RECHERCHE_PRODUCT_DASHBOARD,
              filterProducts: filterProducts
          });
      }
    },

    deleteActionProduct(productId, filterProducts) {
        this.deleteActionProductFromApi(productId, filterProducts);
    },


  /*******************************************
  ********* End products **********
  *******************************************/

    /*******************************************
     ********* Call API **********
     *******************************************/

    addActionProductFromApi(data) {
        axios.post(
            link+"/products",
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
            .then(res => {

                console.log(res)
                AppDispatcher.dispatch({
                    actionType: AppConstants.ADD_PRODUCT,
                });
            })
            .catch(error => {
                console.log(error)
            }).then(() => {
            console.log("End ADD_PRODUCT..")
        });
    },

    editActionProductFromApi(productId, data) {
        console.log("Begin EDIT_PRODUCT..")
        axios.put(
            link+"/products/"+productId,
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
            .then(res => {
                console.log(res)
                this.getActionProductsFromAfterEditApi()
            })
            .catch(error => {
                console.log(error)
            }).then(() => {
            console.log("End EDIT_PRODUCT..")
        });
    },

    addActionCategoryProductFromApi(productId, data) {
        console.log("Begin ADD_CATEGORY_PRODUCT..")
        axios.put(
            link+"/products/"+productId+"/category",
            JSON.stringify(data),
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
            .then(res => {
                console.log(res)
                this.getActionProductsFromAfterEditApi()
            })
            .catch(error => {
                console.log(error)
            }).then(() => {
            console.log("End ADD_CATEGORY_PRODUCT..")
        });
    },

    deleteActionProductFromApi(productId, filterProducts) {
        if(filterProducts.length!=0) clearInterval(intervalTime);
        axios.delete(link+"/products/"+productId)
            .then(res => {
                AppDispatcher.dispatch({
                    actionType: AppConstants.DELETE_PRODUCT,
                    productId:Number(productId)
                });
            })
            .catch(error => {
                console.log(error)
            }).then(() => {
            console.log("End DELETE_PRODUCT..")
        });
    },

    getActionProductsFromApi() {
        axios.get(link+"/products/")
            .then(res => {
                AppDispatcher.dispatch({
                    actionType: AppConstants.LIST_PRODUCTS_FROM_API,
                    datas:res.data
                });
            })
            .catch(error => {
                console.log(error)
            }).then(() => {
        })
    },

    getActionProductsFromAfterEditApi() {
        axios.get(link+"/products/")
            .then(res => {
                AppDispatcher.dispatch({
                    actionType: AppConstants.EDIT_PRODUCT,
                    datas:res.data
                });
            })
            .catch(error => {
                console.log(error)
            }).then(() => {
        })
    },

    getActionBrandsFromApi() {
        axios.get(link+"/brands/")
            .then(res => {
                AppDispatcher.dispatch({
                    actionType: AppConstants.LIST_BRANDS_FROM_API,
                    datas:res.data
                });
            })
            .catch(error => {
                console.log(error)
            }).then(() => {
            console.log("getActionBrandsFromApi")
        })
    },

    getActionCategoriesFromApi() {
        axios.get(link+"/categories/")
            .then(res => {
                AppDispatcher.dispatch({
                    actionType: AppConstants.LIST_CATEGORIES_FROM_API,
                    datas:res.data
                });
            })
            .catch(error => {
                console.log(error)
            })
    },

};

export default AppActions;