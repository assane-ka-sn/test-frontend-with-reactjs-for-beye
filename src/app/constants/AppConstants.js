import keyMirror from 'keymirror';

const AppConstants = keyMirror({
    LIST_PRODUCTS_FROM_API: null,
    LIST_CATEGORIES_FROM_API: null,
    LIST_BRANDS_FROM_API: null,
    LIST_PRODUCTS: null,
    ADD_PRODUCT: null,
    EDIT_PRODUCT: null,
    DELETE_PRODUCT: null,
    RECHERCHE_PRODUCT_DASHBOARD: null,
});

export default AppConstants;