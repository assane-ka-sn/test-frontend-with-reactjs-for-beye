import React from 'react';

const styles = {
    navebar: {
        color: 'white',
    },
};

export default React.createClass({
    render() {
        return (
            <div className="container-fluid">
                <nav className="navbar navbar-expand navbar-dark bg-dark fixed-top"  style={styles.navebar}>
                    <a className="navbar-brand mr-1">TEST FRONTEND REACT FOR BEYE</a>
                </nav>
                {this.props.children}
            </div>
        );
    }
})