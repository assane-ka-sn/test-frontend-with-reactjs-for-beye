import React from 'react';
import { Col, Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { faEdit } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import AppActions from '../../actions/AppActions'
import StoreDashboard from '../../stores/StoreDashboard';


export default class EditProduct extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            brands: [],
            modal: false,
            product: this.props.product,
            brandIdProduct: this.props.product.brand.id,
            nameProduct: this.props.product.name,
            descriptionProduct: this.props.product.description,
        };
        AppActions.getActionBrandsFromApi()

        this._handleEditProduct = this._handleEditProduct.bind(this);

        this._changeBrandIdProduct = this._changeBrandIdProduct.bind(this);
        this._changeDescriptionProduct = this._changeDescriptionProduct.bind(this);
        this._changeProductName = this._changeProductName.bind(this);

        this.openModalEditProduct = this.openModalEditProduct.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    render() {
        const optionBrands = this.state.brands.map(brand => (<option key={brand.id} value={brand.id}>{brand.name}</option>));
        return  (<span>
            <Button onClick={this.openModalEditProduct}  className="btn btn-warning btn-circle btn-sm" title="edit product"><FontAwesomeIcon icon={faEdit} /></Button>
            <Modal isOpen={this.state.modal} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
                   toggle={this.closeModal} className={this.props.className}>
                <ModalHeader toggle={this.closeModal}>Edit product</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup row>
                            <Label for="newProductName" sm={2}>Name</Label>
                            <Col sm={10}>
                                <Input type="text" name="newProductName" value={this.state.nameProduct} onChange={this._changeProductName} id="newProductName" placeholder="Enter name" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="newDescriptionProduct" sm={2}>Description</Label>
                            <Col sm={10}>
                                <Input type="textarea" name="text"  value={this.state.descriptionProduct} onChange={this._changeDescriptionProduct} id="newDescriptionProduct" placeholder="Enter description" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="newBrandIdProduct" sm={2}>Brand</Label>
                            <Col sm={10}>
                                <Input type="select" name="newBrandIdProduct" id="newBrandIdProduct"  value={this.state.brandIdProduct} onChange={this._changeBrandIdProduct}>
                                    <option value="0">veuillez-choisir</option>
                                    {optionBrands}
                                </Input>
                            </Col>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 2 }}>
                            <Button className="btn btn-outline-success my-2 my-sm-0" color="primary" onClick={this._handleEditProduct}>Valid</Button>{' '}
                        </Col>
                    </FormGroup>
                    <Button color="secondary" onClick={this.closeModal}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </span>)
    }

    closeModal() {
        this.setState({
            brandIdProduct: this.props.product.brand.id,
            nameProduct: this.props.product.name,
            descriptionProduct: this.props.product.description,
            modal: !this.state.modal
        });
    }

    openModalEditProduct() {
        this.setState({brands: StoreDashboard.getStoreBrands(), modal: !this.state.modal});
    }

    _changeProductName(event) {
        this.setState({nameProduct: event.target.value.trim()})
    }

    _changeDescriptionProduct(event) {
        this.setState({descriptionProduct: event.target.value.trim()});
    }

    _changeBrandIdProduct(event) {
        this.setState({brandIdProduct: Number(event.target.value.trim())});
    }

    _handleEditProduct() {
        AppActions.editActionProductFromApi(this.props.product.id, {name: this.state.nameProduct, description: this.state.descriptionProduct, brandId: Number(this.state.brandIdProduct)})
        this.closeModal()
        AppActions.getActionProductsFromApi()
    }

}
