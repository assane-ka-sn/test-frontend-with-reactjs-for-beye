import React from 'react';
import { Badge, Card, CardText, CardBody, CardFooter, CardTitle, CardSubtitle, Button, Modal, ModalHeader, ModalBody } from 'reactstrap';

import { faEye } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import AddCategoryProduct from './addCategoryProduct'

export default class DetailProduct extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
        };

        this.openModalDetailProduct = this.openModalDetailProduct.bind(this)
        this.closeModal = this.closeModal.bind(this)
    }

    render() {
        const optionCategories = this.props.product.categories.map(category => (<Badge key={category.id}>{category.name}</Badge>));
        return  (<span>
            <Button onClick={this.openModalDetailProduct}  className="btn btn-primary btn-circle btn-sm" title="view product"><FontAwesomeIcon icon={faEye} /></Button>
            <Modal isOpen={this.state.modal} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
                   toggle={this.closeModal} className={this.props.className}>
                <ModalHeader toggle={this.closeModal}>Detail product</ModalHeader>
                <ModalBody>
                    <Card>
                        <CardBody className="text-center">
                          <CardTitle>{this.props.product.name}</CardTitle>
                          <CardSubtitle>Brand {this.props.product.brand.name}</CardSubtitle>
                          <CardText>{this.props.product.description}</CardText>
                        </CardBody>
                        <CardFooter>
                            <CardSubtitle><AddCategoryProduct key={this.props.product.id} product={this.props.product}/> </CardSubtitle>
                            <CardText>{optionCategories}</CardText>
                        </CardFooter>
                      </Card>
                </ModalBody>
            </Modal>
        </span>)
    }

    closeModal() {
        this.setState({
            modal: !this.state.modal
        });
    }

    openModalDetailProduct() {
        this.setState({
            modal: !this.state.modal
        });
    }

}
