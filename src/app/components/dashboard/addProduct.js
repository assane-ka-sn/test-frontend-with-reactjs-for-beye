import React from 'react';
import { Col, Button, Form, FormGroup, Label, Input, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import AppActions from '../../actions/AppActions'
import StoreDashboard from '../../stores/StoreDashboard';


const styles = {
    buttonaddproduct: {
        margin: 'auto 0',
        textAlign: 'right',
    },
};

export default class AddProduct extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            brands: [],
            modal: false,
            brandIdProduct: 0,
            nameProduct: '',
            descriptionProduct: '',
        };
        AppActions.getActionBrandsFromApi()

        this._handleAddProduct = this._handleAddProduct.bind(this);

        this._changeBrandIdProduct = this._changeBrandIdProduct.bind(this);
        this._changeDescriptionProduct = this._changeDescriptionProduct.bind(this);
        this._changeProductName = this._changeProductName.bind(this);

        this.openModalAddProduct = this.openModalAddProduct.bind(this);
        this.closeModal = this.closeModal.bind(this);

    }

    render() {
        const optionBrands = this.state.brands.map(brand => (<option key={brand.id} value={brand.id}>{brand.name}</option>));
        return  (<div  className="col-9" style={styles.buttonaddproduct}>
            <Button onClick={this.openModalAddProduct} className="btn btn-success btn-circle btn-sm" title="add product"><FontAwesomeIcon icon={faPlusCircle} /> Product</Button>
            <Modal isOpen={this.state.modal} modalTransition={{ timeout: 700 }} backdropTransition={{ timeout: 1300 }}
                   toggle={this.closeModal} className={this.props.className}>
                <ModalHeader toggle={this.closeModal}>New product</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup row>
                            <Label for="newProductName" sm={2}>Name</Label>
                            <Col sm={10}>
                                <Input type="text" name="newProductName"  onChange={this._changeProductName} id="newProductName" placeholder="Enter name" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="newDescriptionProduct" sm={2}>Description</Label>
                            <Col sm={10}>
                                <Input type="textarea" name="text"  onChange={this._changeDescriptionProduct} id="newDescriptionProduct" placeholder="Enter description" />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="newBrandIdProduct" sm={2}>Brand</Label>
                            <Col sm={10}>
                                <Input type="select" name="newBrandIdProduct" id="newBrandIdProduct" onChange={this._changeBrandIdProduct}>
                                    <option value="0">veuillez-choisir</option>
                                    {optionBrands}
                                </Input>
                            </Col>
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <FormGroup check row>
                        <Col sm={{ size: 10, offset: 2 }}>
                            <Button className="btn btn-outline-success my-2 my-sm-0" color="primary" onClick={this._handleAddProduct}>Add</Button>{' '}
                        </Col>
                    </FormGroup>
                    <Button color="secondary" onClick={this.closeModal}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>)
    }

    closeModal() {
        this.setState({
            modal: !this.state.modal
        });
    }

    openModalAddProduct() {
        this.setState({brands: StoreDashboard.getStoreBrands(), modal: !this.state.modal});
    }

    _changeProductName(event) {
        this.setState({nameProduct: event.target.value.trim()})
    }

    _changeDescriptionProduct(event) {
        this.setState({descriptionProduct: event.target.value.trim()});
    }

    _changeBrandIdProduct(event) {
        this.setState({brandIdProduct: Number(event.target.value.trim())});
    }

    _handleAddProduct() {
        AppActions.addActionProductFromApi({name: this.state.nameProduct, description: this.state.descriptionProduct, brandId: Number(this.state.brandIdProduct)})
        this.closeModal()
        AppActions.getActionProductsFromApi()
    }

}
