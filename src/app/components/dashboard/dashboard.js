import React from 'react';

import { faTable } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import ListProduit from './listProduct'
import AddProduct from './addProduct'

const styles = {
    container: {
        marginTop: '5rem',
    },
    buttonaddproduct: {
        margin: 'auto 0',
        textAlign: 'right',
    },
};

export default React.createClass({
    render() {
        return  (<div className="container-fluid" style={styles.container}>
            <div className="card-header row">
                <div className="col-3"><FontAwesomeIcon icon={faTable} /> List brands</div>
                <AddProduct />
            </div>
            <div className="card-body">
                <ListProduit />
            </div>
        </div>)
    }
})