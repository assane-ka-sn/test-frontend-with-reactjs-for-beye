import React from 'react';
import { Input, Button, InputGroup, InputGroupAddon } from 'reactstrap';

import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import AppActions from '../../actions/AppActions'
import StoreDashboard from '../../stores/StoreDashboard';

export default class AddCategoryProduct extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            selectList: false,
            categoryIdProduct: 0,
            inputOpen: false
        };
        AppActions.getActionCategoriesFromApi()

        this.openToggle = this.openToggle.bind(this);
        this.closeToggle = this.closeToggle.bind(this);
        this._changeCategoryIdProduct = this._changeCategoryIdProduct.bind(this);
        this._handleAddCategoryProduct = this._handleAddCategoryProduct.bind(this);
    }

    render() {
        const optionCategories = this.state.categories
            .map(category => {
                if(!this.props.product.categories.find(cat => cat.id==category.id)) return (<option key={category.id} value={category.id}>{category.name}</option>);
            });
        return  (
            <InputGroup  size="sm" >Categories <FontAwesomeIcon icon={faPlusCircle} onClick={this.openToggle} />
                {this.state.inputOpen && <Input type="select" name="newCategoryIdProduct" id="newCategoryIdProduct"  onChange={this._changeCategoryIdProduct}>
                    <option value="0">veuillez-choisir</option>
                    {optionCategories}
                </Input>}
                {this.state.inputOpen && <Button size="sm" color="primary"  onClick={this._handleAddCategoryProduct}>Add</Button>}
            </InputGroup>
        )
    }

    openToggle() {
        this.setState({ categories: StoreDashboard.getStoreCategories(), inputOpen: !this.state.inputOpen });
    }

    closeToggle() {
        this.setState({ inputOpen: !this.state.inputOpen });
    }

    _changeCategoryIdProduct(event) {
        this.setState({categoryIdProduct: Number(event.target.value.trim())});
    }


    _handleAddCategoryProduct() {
        AppActions.addActionCategoryProductFromApi(this.props.product.id, {categoryId: Number(this.state.categoryIdProduct)})
        this.closeToggle()
        AppActions.getActionProductsFromApi()
    }

}
