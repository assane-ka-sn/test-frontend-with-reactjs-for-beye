import React from 'react';
import { faEye, faEdit, faTimes } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import DetailProduct from './detailProduct'
import EditProduct from './editProduct'


export default class RowListProduit extends React.Component {

    constructor(props) {
        super(props);
    }


    render() {
        return  (<tr>
            <td>{this.props.product.id}</td>
            <td>{this.props.product.name}</td>
            <td>{this.props.product.description}</td>
            <td>{this.props.product.brand.name}</td>
            <td className="custom-control-inline">
                <DetailProduct  product={this.props.product} />
                <EditProduct  product={this.props.product} />
                <button type="button" className="btn btn-danger btn-circle btn-sm" onClick={this.props.deleteProduct} title="delete product"><FontAwesomeIcon icon={faTimes} /></button>
            </td>
        </tr>)
    }
}
