import React from 'react';

import RowListProduit from './rowListProduct'

import AppActions from '../../actions/AppActions'
import StoreDashboard from '../../stores/StoreDashboard';

export default class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [],
            filterProducts:'',
        };
        AppActions.getActionInitDashboard()
        this.handleChange = this.handleChange.bind(this);

        this._handleDeleteProduct = this._handleDeleteProduct.bind(this);
        this._handleChangeSearchProduct = this._handleChangeSearchProduct.bind(this);
    }

    handleChange() {
        console.log("list handleChange")
        this.setState({data: StoreDashboard.getStoreProducts()});
    }

    componentDidMount() {
        console.log("list componentDidMount")
        StoreDashboard.addChangeListener(this.handleChange);
    }

    componentWillUnmount() {
        console.log("list componentWillUnmount")
        StoreDashboard.removeChangeListener(this.handleChange);
    }

    render() {
        const rowProducts = this.state.data.map(product => (<RowListProduit key={product.id} deleteProduct={this._handleDeleteProduct.bind(this, product.id)} product={product} />));
        return  (<table className="table table-hover">
                    <thead>
                        <tr><th>#</th><th>Name</th><th>Description</th><th>Brand</th><th>Action</th></tr>
                        <tr>
                            <th>Search: </th>
                            <th colSpan="3">
                                <input id="rechercheProducts" className="form-control" placeholder="Search " onChange={this._handleChangeSearchProduct} />
                            </th>
                        </tr>
                    </thead>
                    <tbody>{rowProducts}</tbody>
                </table>)
    }

    _handleDeleteProduct(productId) {
        console.log(this.state.filterProducts)
        AppActions.deleteActionProduct(Number(productId), this.state.filterProducts)
    }

    _handleChangeSearchProduct(event) {
        this.setState({filterProducts: event.target.value.trim()});
        AppActions.getActionRechercheProductsDashboard(event.target.value.trim())
    };

}
